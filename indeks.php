
<!DOCTYPE html>
<html>
<head>
    <title>yoggalih_Sign Up</title>
    <link rel="stylesheet" type="text/css" href="login.css">
</head>
<body>
    <div class="container">
        <h1>Sign Up</h1>

        <?php
        // Array untuk menyimpan data pengguna
        $users = array();

        // Error array untuk validasi
        $errors = array();

        // Proses pendaftaran (Sign Up)
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            // Validasi input nama
            if (empty($_POST["name"])) {
                $errors["name"] = "Nama harus diisi";
            } else {
                $name = $_POST["name"];
            }

            // Validasi input email
            if (empty($_POST["email"])) {
                $errors["email"] = "Email harus diisi";
            } else {
                // Gunakan fungsi filter_var() untuk memvalidasi format email
                if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
                    $errors["email"] = "Format email tidak valid";
                } else {
                    $email = $_POST["email"];
                }
            }

            // Validasi input kata sandi
            if (empty($_POST["password"])) {
                $errors["password"] = "Kata sandi harus diisi";
            } else {
                $password = $_POST["password"];
            }

            // Jika tidak ada error, simpan data pengguna
            if (empty($errors)) {
                // Simpan data pengguna ke dalam array
                $user = array(
                    "name" => $name,
                    "email" => $email,
                    "password" => $password
                );

                $users[] = $user;

                // Redirect ke halaman login setelah pendaftaran berhasil
                header("Location: login.php");
                exit;
            }
        }
        ?>

        <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
            <label for="name">Nama:</label>
            <input type="text" id="name" name="name" value="<?php echo isset($name) ? $name : ''; ?>">
            <span class="error"><?php echo isset($errors['name']) ? $errors['name'] : ''; ?></span>

            <label for="email">Email:</label>
            <input type="text" id="email" name="email" value="<?php echo isset($email) ? $email : ''; ?>">
            <span class="error"><?php echo isset($errors['email']) ? $errors['email'] : ''; ?></span>

            <label for="password">Kata Sandi:</label>
            <input type="password" id="password" name="password">
            <span class="error"><?php echo isset($errors['password']) ? $errors['password'] : ''; ?></span>

            <input type="submit" class="submit-btn" value="Sign Up">
        </form>
    </div>
</body>
</html>
