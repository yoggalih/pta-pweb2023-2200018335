<!DOCTYPE html>
<html>
<head>
    <title>yoggalih_Login</title>
    <link rel="stylesheet" type="text/css" href="login.css">
</head>
<body>
    <div class="container">
        <h1>Login</h1>

        <?php
        // Proses login
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            // Validasi input email
            if (empty($_POST["email"])) {
                $errors["email"] = "Email harus diisi";
            } else {
                // Gunakan fungsi filter_var() untuk memvalidasi format email
                if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
                    $errors["email"] = "Format email tidak valid";
                } else {
                    $email = $_POST["email"];
                }
            }

            // Validasi input kata sandi
            if (empty($_POST["password"])) {
                $errors["password"] = "Kata sandi harus diisi";
            } else {
                $password = $_POST["password"];
            }

            // Jika tidak ada error, lakukan proses login
            if (empty($errors)) {
                // Misalnya, lakukan validasi dengan data pengguna yang ada
                $validUser = false;

                // Simulasi data pengguna
                $users = array(
                    array(
                        "email" => "yogagalih626@gmail.com",
                        "password" => "123"
                    ),
                    array(
                        "email" => "user2@example.com",
                        "password" => "password2"
                    )
                );

                foreach ($users as $user) {
                    if ($user["email"] == $email && $user["password"] == $password) {
                        $validUser = true;
                        break;
                    }
                }

                if ($validUser) {
                    // Redirect ke halaman indeks setelah login berhasil
                    header("Location: dasboard.html");
                    exit;
                } else {
                    $errors["login"] = "Email atau kata sandi salah";
                }
            }
        }
        ?>

        <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
            <label for="email">Email:</label>
            <input type="text" id="email" name="email" value="<?php echo isset($email) ? $email : ''; ?>">
            <span class="error"><?php echo isset($errors['email']) ? $errors['email'] : ''; ?></span>

            <label for="password">Kata Sandi:</label>
            <input type="password" id="password" name="password">
            <span class="error"><?php echo isset($errors['password']) ? $errors['password'] : ''; ?></span>

            <span class="error"><?php echo isset($errors['login']) ? $errors['login'] : ''; ?></span>

            <input type="submit" class="submit-btn" value="Login">
        </form>
    </div>
</body>
</html>
